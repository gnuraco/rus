# Альтернативный перевод интерфейса

Модуль для перевода интерфейса VTT Foundry на русский язык.

Требуемая версия FoundryVTT: V11/V12

**Установка по манифесту V11 версия:** https://gitlab.com/gnuraco/rus/-/raw/V11/module.json

**Установка по манифесту V12 версия:** https://gitlab.com/gnuraco/rus/-/raw/V12/module.json
